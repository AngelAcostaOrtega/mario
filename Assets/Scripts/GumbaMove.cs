﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GumbaMove : MonoBehaviour
{
    [SerializeField] [Range(-1, 1)] private float x;
    private float gx;
    [SerializeField] private float velocity;
    private float count;
    private bool recoil;

    private Vector2 pos1;
    private Vector2 pos2;
    private float tempo;
    private void Start()
    {
        if (x == 0) x = 0;
        if (velocity == 0) velocity = 1;

        pos1 = Vector2.zero;
        pos2 = Vector2.zero;

        count = 0.0f;
        tempo = 0.0f;
        gx = 0;

        recoil = false;
    }
    void Update()
    {
        this.TryGetComponent(out Rigidbody2D rb);
        this.TryGetComponent(out Animator an);
        this.TryGetComponent(out SpriteRenderer sr);
        pos1 = this.transform.position;


        if(x != 0)
        {
            sr.flipX = x < 0 ? true : false;
            an.SetBool("mover", true);
            an.SetBool("correr", true);
            rb.velocity = (Vector3)(new Vector2(x * velocity, rb.velocity.y));
            if (pos1 == pos2) rb.velocity = (Vector3)(new Vector2(rb.velocity.y, rb.velocity.y + 0.5f));
            if (tempo >= 0.5f)
            {
                tempo = 0.0f;
                pos2 = this.transform.position;
            }
            tempo += Time.deltaTime;
        }
        else
        {
            tempo = 0.0f;
            if (an.GetBool("correr")) an.SetBool("correr", false);
            if (an.GetBool("mover")) an.SetBool("mover", false);
        }

        if (recoil)
        {
            count += Time.deltaTime;
            if(count >= 0.5f)
            {
                an.SetBool("atacar", false);
                x = gx;
            }
        }
    }
    public bool collision(Vector2 direction)
    {
        this.TryGetComponent(out BoxCollider2D bc);

        Vector2 pos = Vector2.zero;
        float longitud = 0.1f;
        pos.x = (bc.bounds.extents.x) * direction.x ;
        pos.y = bc.bounds.extents.y;
        return Physics2D.Raycast((this.gameObject.transform.position + (Vector3) pos), direction, longitud) ? true : false;
    }
    public bool jumpcollisions(Vector2 direction)
    {
        this.TryGetComponent(out BoxCollider2D bc);

        Vector2 pos = Vector2.zero;
        float longitud = 0.1f;
        if (direction.x != 0 && direction.y == 0)
        {
            pos = new Vector2(this.transform.position.x + ((bc.bounds.extents.x + 0.1f) * direction.x),
                                      this.transform.position.y + ((bc.bounds.extents.y) * 1));
            direction = Vector2.down;
            longitud = bc.bounds.size.y;
        }
        else if (direction.y != 0 && direction.x == 0)
        {
            pos = new Vector2(this.transform.position.x + ((bc.bounds.extents.x) * -1),
                                  this.transform.position.y + ((bc.bounds.extents.y + 0.1f) * direction.y));
            direction = Vector2.right;
            longitud = bc.bounds.size.x;
        }
        if (Physics2D.Raycast(pos, direction, longitud))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private void Attack(Vector2 Mario)
    {
        this.TryGetComponent(out SpriteRenderer sr);
        this.TryGetComponent(out Animator an);

        gx = x;
        x = 0;
        sr.flipX = (Mario.x < this.transform.position.x ? true : false);
        an.SetBool("atacar", true);
        count = 0.0f;
        recoil = true;
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        this.TryGetComponent(out ParticleSystem ps);
        this.TryGetComponent(out BoxCollider2D bc);
        this.TryGetComponent(out SpriteRenderer sr);
        this.TryGetComponent(out Animator an);

        if ( !col.gameObject.CompareTag("Player"))
        {
            Boolean flag = collision(x >= 0 ? Vector2.right : Vector2.left);
            if ( flag )
            {
                x = -x;
            }
        }
        else
        {
            col.gameObject.TryGetComponent(out Rigidbody2D Mrb);
            Boolean flag = jumpcollisions(Vector2.up);
            if (flag)
            {
                Mrb.AddRelativeForce(new Vector2(0,
                                                500 * 1.25f) * 0.014f,
                                                ForceMode2D.Impulse);
                x = 0;
                ps.Play();
                Camera.main.GetComponent<GameMotor>().sumaPuntos(20);
                Destroy(sr, 0.01f);
                Destroy(bc, 0.01f);
                Destroy(this.gameObject, 0.4f);
                Destroy(this, 0.01f);
            }
            else
            {
                Attack(col.transform.position);
                Mrb.AddRelativeForce(new Vector2(0,
                                                300 * 1.25f) * 0.014f,
                                                ForceMode2D.Impulse);
                col.gameObject.GetComponent<Jugador>().restarVida();
            }
        }
    }
}
