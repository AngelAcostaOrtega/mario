﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    //[SerializeField][Range(0, 100)]private float cameraspeed;
    private Transform pp;
    private Rigidbody2D prb;
    private void Start()
    {
        pp = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        prb = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        this.TryGetComponent(out Transform cp);

        Vector2 limitemaximo = Camera.main.ScreenToWorldPoint( (Vector3)(new Vector2(
                                                                        ((Camera.main.scaledPixelWidth / 6) * 4),
                                                                        ((Camera.main.scaledPixelHeight / 6) * 5))) );
        Vector2 limiteminimo = Camera.main.ScreenToWorldPoint( (Vector3)(new Vector2(
                                                                        ((Camera.main.scaledPixelWidth / 6) * 2),
                                                                        ((Camera.main.scaledPixelHeight / 6) * 2))) );
        float direction = 0;
        if(pp.position.x > limitemaximo.x)
        {
            direction = (prb.velocity.x >= 0) ? prb.velocity.x : prb.velocity.x * -1;
            cp.Translate(Vector2.right * direction * Time.deltaTime);
        }
        else if(pp.position.x < limiteminimo.x)
        {
            direction = (prb.velocity.x >= 0) ? prb.velocity.x : prb.velocity.x * -1;
            cp.Translate(Vector2.left * direction * Time.deltaTime);
        }
        if(pp.position.y > limitemaximo.y)
        {
            direction = (prb.velocity.y >= 0) ? prb.velocity.y : prb.velocity.y * -1;
            cp.Translate(Vector2.up * direction * Time.deltaTime);
        }
        else if(pp.position.y < limiteminimo.y)
        {
            direction = (prb.velocity.y >= 0) ? prb.velocity.y : prb.velocity.y * -1;
            cp.Translate(Vector2.down * direction * Time.deltaTime);
        }

    }
}
