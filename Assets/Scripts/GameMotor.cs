﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMotor : MonoBehaviour
{
    int puntos;
    private void Start()
    {
        puntos = 0;
        Time.timeScale = 1;
    }
    public void cambiaVida(int n)
    {
        GameObject.Find("GUI/Canvas/InGame/TVidas").GetComponent<TMPro.TextMeshProUGUI>().text = "Vidas: " + n.ToString();
    }
    public void sumaPuntos(int n)
    {
        puntos += n;
        GameObject.Find("GUI/Canvas/InGame/TPuntos").GetComponent<TMPro.TextMeshProUGUI>().text = "Puntos: " + puntos.ToString();
    }

    public void lose()
    {
        GUIUpdate("lose");
    }
    public void win()
    {
        GUIUpdate("win");
    }
    public void pause()
    {
        GUIUpdate("pause");
    }

    private void GUIUpdate(string n)
    {
        Time.timeScale = 0;
        GameObject.Find("GUI/Canvas/InGame").SetActive(false);
        GameObject.Find("GUI/Canvas/"+n+"/LPuntos").GetComponent<TMPro.TextMeshProUGUI>().text = GameObject.Find("GUI/Canvas/InGame/TPuntos").GetComponent<TMPro.TextMeshProUGUI>().text;
        GameObject.Find("GUI/Canvas/"+n).SetActive(true);
    }
}
