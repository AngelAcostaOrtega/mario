﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private AudioSource speaker;
    private void Start()
    {
        speaker = this.gameObject.GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if( col.gameObject.CompareTag("Player") )
        {
            Camera.main.GetComponent<GameMotor>().sumaPuntos(10);
            Destroy(this.gameObject.GetComponent<SpriteRenderer>(), 0.02f);
            speaker.Play();
            Destroy(this.gameObject, speaker.clip.length + 0.01f);
        }
    }
}
